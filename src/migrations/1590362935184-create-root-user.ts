import {MigrationInterface, QueryRunner} from 'typeorm';

export class createRootUser1590362935184 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`INSERT INTO "users" (login, password) VALUES ( 'root', '4813494d137e1631bba301d5acab6e7bb7aa74ce1185d456565ef51d737677b2')`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DELETE FROM "users" WHERE login = 'root'`);
    }

}
