import * as bodyParser from 'body-parser';
import * as express from 'express';
import { createExpressServer, useContainer as useRouteContainer } from 'routing-controllers';
import { useContainer as useTypeormContainer } from 'typeorm';
import { Container } from 'typedi';
import { config } from './config';
import { logger } from './logger';
import {DataBase} from './database';
import {ErrorHandlerMiddleware, SetHeadersMiddleware} from '@middlewares';
import {BooksController, UsersController} from '@controllers';
import {authorizationChecker} from './services/helpers/authentication';

export class App {
    public app: express.Application;

    constructor(private database: DataBase) {
        useRouteContainer(Container);
        useTypeormContainer(Container);

        this.app = createExpressServer({
            routePrefix: '/api',
            defaultErrorHandler: false,
            classTransformer: true,
            validation: true,
            controllers: [
                UsersController,
                BooksController
            ],
            middlewares: [
                bodyParser.urlencoded({ extended: true }),
                bodyParser.json(),
                SetHeadersMiddleware,
                ErrorHandlerMiddleware
            ],
            authorizationChecker
        });

        database.connect();

        this.app.listen(config.port, () => {
            logger.info(`Listening at http://${config.host}:${config.port}/`);
        });
    }
}

export const app = new App(Container.get(DataBase)).app;
