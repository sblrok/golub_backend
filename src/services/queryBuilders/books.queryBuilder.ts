import {Service} from 'typedi';
import {BaseQueryBuilder} from './baseQueryBuilder';
import {SelectQueryBuilder} from 'typeorm';
import {Book} from '@models';

export class BooksQueryBuilder extends BaseQueryBuilder<Book> {
    public dictionary: Partial<{ [key in keyof Book]: string }> = {
        id: '"Book"."id"',
        title: '"Book"."title"',
        description: '"Book"."description"',
        author: '"Book"."author"',
        year: '"Book"."year"',
        image: '"Book"."image"',
        publisher: '"Book"."publisher"',
    };
    protected queryBuilder: SelectQueryBuilder<Book> = Book
        .getRepository()
        .createQueryBuilder()
        .orderBy('"Book"."title"', 'ASC');
}
