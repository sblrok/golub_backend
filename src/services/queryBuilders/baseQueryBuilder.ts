import { BaseEntity, SelectQueryBuilder } from 'typeorm';
import {IQueryBuilder} from '@interfaces';

export abstract class BaseQueryBuilder<TEntity> implements IQueryBuilder<TEntity> {
    public abstract dictionary: Partial<{ [key in keyof TEntity]: string }>;
    protected abstract queryBuilder: SelectQueryBuilder<TEntity>;

    public where(model: Partial<{ [key in keyof TEntity]: TEntity[key] | Array<TEntity[key]> }>): InstanceType<new () => this>;
    public where(key: keyof TEntity, value: {}): InstanceType<new () => this>;
    public where(keyOrModel: Partial<{ [key in keyof TEntity]: TEntity[key] | Array<TEntity[key]> }> | keyof TEntity, value?: {}): InstanceType<new () => this> {
        const model = (typeof keyOrModel === 'string' && { [keyOrModel]: value }) || keyOrModel;

        if (model === undefined) {
            return this;
        }

        for (const key of (model || []) && Object.keys(model)) {
            if (Array.isArray(model[key])) {
                this.queryBuilder = this.queryBuilder
                    .andWhere(`${this.dictionary[key]} IN ('${model[key].join('\', \'')}')`);
            } else {
                this.queryBuilder = this.queryBuilder
                    .andWhere(`${this.dictionary[key]} = :${key}`, {
                        [key]: model[key]
                    });
            }
        }

        return this;
    }

    public join(name: keyof TEntity): BaseQueryBuilder<TEntity> {
        throw new Error('ERR_METHOD_NOT_IMPLEMENTED');
        return this;
    }

    public limit(count: number) {
        this.queryBuilder = this.queryBuilder.take(count);
        return this;
    }

    public offset(count: number) {
        this.queryBuilder = this.queryBuilder.skip(count);
        return this;
    }

    public getQuery() {
        return this.queryBuilder;
    }
}

