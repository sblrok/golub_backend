import {Service} from 'typedi';
import {BaseQueryBuilder} from './baseQueryBuilder';
import {User} from '@models';
import {SelectQueryBuilder} from 'typeorm';

export class UsersQueryBuilder extends BaseQueryBuilder<User> {
    public dictionary: Partial<{ [key in keyof User]: string }> = {
        id: '"User"."id"',
        login: '"User"."login"',
        password: '"User"."password"',
    };
    protected queryBuilder: SelectQueryBuilder<User> = User
        .getRepository()
        .createQueryBuilder()
        .orderBy('"User"."login"', 'ASC');

    public selectPassword() {
        this.queryBuilder.addSelect('"User"."password"', 'User_password');
        return this;
    }
}
