import { Service } from 'typedi';
import {ExceptionTypes} from '@enums';
import {IRequestResult} from '@interfaces';

@Service()
export default class RequestResultFactory {
  private resultDictionary: Partial<{ [key in ExceptionTypes]: IRequestResult }> = {
    [ExceptionTypes.NotFound]: { code: 404, message: 'Not Found' },
    [ExceptionTypes.BadRequest]: { code: 400, message: 'Bad Request' },
    [ExceptionTypes.InternalServerError]: { code: 500, message: 'Internal Server Error' },
    [ExceptionTypes.ForbiddenError]: { code: 403, message: 'Forbidden' },
    [ExceptionTypes.MethodNotAllowedError]: { code: 405, message: 'Method Not Allowed' },
    [ExceptionTypes.UnauthorizedError]: { code: 403, message: 'Unauthorized' },
    [ExceptionTypes.NotAcceptableError]: { code: 406, message: 'Not Acceptable Error' }
  };
  constructor() {}

  public createResult(type: ExceptionTypes, message?: string): IRequestResult {
    const result = this.resultDictionary[type] || this.resultDictionary[ExceptionTypes.InternalServerError];
    result.message = message || result.message;
    return result;
  }
}
