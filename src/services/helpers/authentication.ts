import {Container} from 'typedi';
import {Action} from 'routing-controllers';
import {UsersService} from '../users.service';
import * as crypto from 'crypto';

export async function authorizationChecker(action: Action, roles: string[]) {
    // TODO: implement jwt or something
    const login = action.request.headers.login;
    const password = action.request.headers.password;

    const usersService = Container.get(UsersService);

    const user = await usersService.getUserPassword({searchBy: {login}});

    if (!user) {
        // throw new BaseException(ExceptionTypes.ForbiddenError);
        return false;
    }

    const hashedPassword = crypto
        .createHash('sha256')
        .update(user.password)
        .digest('hex')

    if (hashedPassword !== password) {
        // throw new BaseException(ExceptionTypes.ForbiddenError);
        return false;
    }

    return true;
}

