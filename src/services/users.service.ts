import {Service} from 'typedi';
import {User} from '@models';
import {IFindOptions} from '@interfaces';
import {UsersQueryBuilder} from '@queryBuilders';
import {ServiceException} from '@exceptions';
import {ExceptionTypes} from '@enums';

@Service()
export class UsersService {
    public find(options: IFindOptions<User>) {
        const builder = new UsersQueryBuilder()
            .limit(options.limit)
            .offset(options.offset)
            .where(options.searchBy)
            .getQuery();


        return builder.getMany();
    }

    public findOne(options: IFindOptions<User>) {
        const builder = new UsersQueryBuilder()
            .where(options.searchBy)
            .getQuery();

        return builder.getOne();
    }

    public getUserPassword(options: IFindOptions<User>) {
        const builder = new UsersQueryBuilder()
            .where(options.searchBy)
            .selectPassword()
            .getQuery();

        return builder.getOne();
    }

    public async create(user: User) {
        await this.checkUserByUniqueFields(user, 'Unique fields is already taken')

        return User.create(user);
    }

    public async update(options: IFindOptions<User>, user: User) {
        await this.checkUserByUniqueFields(user, 'Unique fields is already taken')

        await this.checkUserById(options.searchBy.id, 'User doesn`t exists');

        return User.update(options.searchBy.id, user);
    }

    public async delete(options: IFindOptions<User>) {
        await this.checkUserById(options.searchBy.id, 'User doesn`t exists');

        await User.delete(options.searchBy.id);
    }

    private async checkUserByUniqueFields(user: User, errorMessage: string) {
        const uniqueFields = {
            login: user.login
        };

        const builder = new UsersQueryBuilder()
            .where(uniqueFields)
            .getQuery();

        const existed = await builder.getOne();

        if (existed) {
            throw new ServiceException(
                ExceptionTypes.AlreadyExistsError,
                errorMessage
            );
        }
    }

    private async checkUserById(id: string | string[], errorMessage: string) {
        const builder = new UsersQueryBuilder()
            .where({id})
            .getQuery();

        const existed = await builder.getOne();

        if (existed) {
            throw new ServiceException(
                ExceptionTypes.NotFound,
                errorMessage);
        }
    }
}
