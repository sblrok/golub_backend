import { registerDecorator, ValidationArguments, ValidationOptions } from 'class-validator';

export function ShouldContainObject(property: object, validationOptions?: ValidationOptions) {
  return (object: object, propertyName: string) => {
    validationOptions = validationOptions || {};
    validationOptions.message = validationOptions.message || 'Value should contain ' + JSON.stringify(property);
    registerDecorator({
      name: 'shouldContainObject',
      target: object.constructor,
      propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: object, args: ValidationArguments) {
          const validateBy = args.constraints[0];
          return validateByObject(value, validateBy);
        }
      }
    });
  };
}

function validateByObject(value: object, validateBy: object) {
  const keys = Object.keys(validateBy);
  let notValid = false;
  keys.forEach(key => {
    if (typeof validateBy[key] === 'object') {
      if (!value[key] || typeof value[key] !== 'object') {
        notValid = true;
      }
      notValid = notValid || !validateByObject(value[key], validateBy[key]);
    } else if (value[key] === undefined || typeof value[key] !== validateBy[key]) {
      notValid = true;
    }
  });

  return !notValid;
}
