import { Request, Response } from 'express';
import {
  BadRequestError,
  ForbiddenError,
  MethodNotAllowedError,
  Middleware,
  NotAcceptableError,
  NotFoundError,
  UnauthorizedError
} from 'routing-controllers';
import { ExpressErrorMiddlewareInterface } from 'routing-controllers/driver/express/ExpressErrorMiddlewareInterface';
import { logger } from '../../logger';
import { ExceptionTypes } from '@enums';
import {BaseException} from '@exceptions';
import RequestResultFactory from '../helpers/requestResultFactory';

@Middleware({ type: 'after' })
export class ErrorHandlerMiddleware implements ExpressErrorMiddlewareInterface {
  constructor(private requestResultFactory: RequestResultFactory) {}

  public error(err: BaseException | Error, request: Request, response: Response, next?: (err?: Error) => void) {
    let result;
    if (err instanceof Error) {
      let type = ExceptionTypes.InternalServerError;
      if (err instanceof NotFoundError) {
        type = ExceptionTypes.NotFound;
      } else if (err instanceof BadRequestError) {
        type = ExceptionTypes.BadRequest;
      } else if (err instanceof ForbiddenError) {
        type = ExceptionTypes.ForbiddenError;
      } else if (err instanceof MethodNotAllowedError) {
        type = ExceptionTypes.MethodNotAllowedError;
      } else if (err instanceof UnauthorizedError) {
        type = ExceptionTypes.UnauthorizedError;
      } else if (err instanceof NotAcceptableError) {
        type = ExceptionTypes.NotAcceptableError;
      }

      let message = err.message;

      // @ts-ignore
      const { errors } = err;
      if (errors && errors.length) {
        message = errors
          .map(error =>
            Object.keys(error.constraints)
              .map(key => error.constraints[key])
              .join(', ')
          )
          .join(', ');
      }
      const debugMessage = err.stack;
      result = this.requestResultFactory.createResult(type, message);
      logger.error(type + ': ' + message);
      logger.debug(debugMessage);
    } else {
      result = this.requestResultFactory.createResult(err.getHttpType(), err.getBaseMessage());
      logger.error('Manual error: ' + result.message);
      logger.debug(err.getTrace());
    }
    response.status(result.code).json(result);
  }
}
