import {Service} from 'typedi';
import {Book} from '@models';
import {IFindOptions} from '@interfaces';

import {ServiceException} from '@exceptions';
import {ExceptionTypes} from '@enums';
import {BooksQueryBuilder} from './queryBuilders/books.queryBuilder';

@Service()
export class BooksService {
    public find(options: IFindOptions<Book>) {
        const builder = new BooksQueryBuilder()
            .limit(options.limit)
            .offset(options.offset)
            .where(options.searchBy)
            .getQuery();

        return builder.getMany();
    }

    public findOne(options: IFindOptions<Book>) {
        const builder = new BooksQueryBuilder()
            .where(options.searchBy)
            .getQuery();

        return builder.getOne();
    }

    public async create(book: Book) {
        await this.checkBookByUniqueFields(book, 'Unique fields is already taken');

        return Book.create(book);
    }

    public async update(options: IFindOptions<Book>, book: Book) {
        await this.checkBookByUniqueFields(book, 'Unique fields is already taken');

        await this.checkBookById(options.searchBy.id, 'Book doesn`t exists');

        return Book.update(options.searchBy.id, book);
    }

    public async delete(options: IFindOptions<Book>) {
        await this.checkBookById(options.searchBy.id, 'Book doesn`t exists');

        await Book.delete(options.searchBy.id);
    }

    private async checkBookByUniqueFields(book: Book, errorMessage: string) {
        const uniqueFields = {
            title: book.title,
            author: book.author,
            publisher: book.publisher
        };

        const builder = new BooksQueryBuilder()
            .where(uniqueFields)
            .getQuery();

        const existed = await builder.getOne();

        if (existed) {
            throw new ServiceException(
                ExceptionTypes.AlreadyExistsError,
                errorMessage
            );
        }
    }

    private async checkBookById(id: string | string[], errorMessage: string) {
        const builder = new BooksQueryBuilder()
            .where({id})
            .getQuery();

        const existed = await builder.getOne();

        if (existed) {
            throw new ServiceException(
                ExceptionTypes.NotFound,
                errorMessage);
        }
    }
}
