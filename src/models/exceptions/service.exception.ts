import {BaseException} from './baseException';

export class ServiceException extends BaseException {
    public toString(): string {
        return `Service exception: ${this.type}, ${this.message}`;
    }
}
