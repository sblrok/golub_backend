import { ExceptionTypes } from '../enums/exceptionTypes';

export class BaseException {
  public readonly message: string;
  public readonly type: ExceptionTypes;
  public readonly innerException: BaseException | Error;
  private exceptionMap: Partial<{ [key in ExceptionTypes]: ExceptionTypes }> = {
    // [ExceptionTypes.NotFound]: ExceptionTypes.NotFound,
    // [ExceptionTypes.BadRequest]: ExceptionTypes.NotFound,
    // [ExceptionTypes.InternalServerError]: ExceptionTypes.NotFound,
    // [ExceptionTypes.ForbiddenError]: ExceptionTypes.NotFound,
    // [ExceptionTypes.NotAcceptableError]: ExceptionTypes.NotFound,
    // [ExceptionTypes.MethodNotAllowedError]: ExceptionTypes.NotFound,
    // [ExceptionTypes.UnauthorizedError]: ExceptionTypes.NotFound,
    [ExceptionTypes.FindError]: ExceptionTypes.NotFound,
    [ExceptionTypes.CreateError]: ExceptionTypes.InternalServerError,
    [ExceptionTypes.UpdateError]: ExceptionTypes.InternalServerError,
    [ExceptionTypes.DeleteError]: ExceptionTypes.InternalServerError,
    [ExceptionTypes.NotExistsError]: ExceptionTypes.NotFound,
    [ExceptionTypes.AlreadyExistsError]: ExceptionTypes.BadRequest
  };

  constructor(type?: ExceptionTypes, message?: string, innerException?: BaseException | Error) {
    this.type = type || ExceptionTypes.InternalServerError;
    this.message = message || 'Internal Server Error';
    this.innerException = innerException;
  }

  public toString(): string {
    return `${this.type}, ${this.message}`;
  }

  public getBaseMessage(): string {
    if (this.innerException instanceof BaseException) {
      return this.innerException.getBaseMessage();
    }
    return this.message;
  }

  public getBaseType(): ExceptionTypes {
    if (this.innerException instanceof BaseException) {
      return this.innerException.getBaseType();
    }
    return this.type;
  }

  public getHttpType(): ExceptionTypes {
    const baseType = this.getBaseType();
    return this.exceptionMap[baseType] || ExceptionTypes.InternalServerError;
  }

  public getTrace() {
    let message = this.toString();
    if (this.innerException) {
      if (this.innerException instanceof BaseException) {
        message += `\n>${this.innerException.getTrace()}`;
      } else {
        message += `\n>${this.innerException.stack}`;
      }
    }
    return message;
  }
}
