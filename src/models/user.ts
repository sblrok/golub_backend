import {IsNotEmpty, IsUUID, Length,} from 'class-validator';
import {BaseEntity, Column, Entity, PrimaryColumn, PrimaryGeneratedColumn} from 'typeorm';

@Entity({ name: 'users', schema: 'public' })
export class User extends BaseEntity {
    @IsUUID('4')
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @Length(6, 30)
    @IsNotEmpty()
    @Column()
    public login: string;

    @Length(6, 30)
    @IsNotEmpty()
    @Column({ select: false })
    public password: string;
}
