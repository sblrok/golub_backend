export interface IFindOptions<T> {
    searchBy?: { [key in keyof T]?: T[key] | Array<T[key]> };
    limit?: number;
    offset?: number;
}
