export interface IRequestResult {
  code: number;
  message: string;
}
