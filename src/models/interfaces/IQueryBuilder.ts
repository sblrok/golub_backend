import { SelectQueryBuilder } from 'typeorm';
import {BaseQueryBuilder} from '@queryBuilders';

export interface IQueryBuilder<TEntity> {
    dictionary: Partial<{ [key in keyof TEntity]: string }>;

    where(model: Partial<{ [key in keyof TEntity]: TEntity[key] | Array<TEntity[key]> }>): IQueryBuilder<TEntity>;
    where(key: keyof TEntity, value: {}): IQueryBuilder<TEntity>;
    join(name: keyof TEntity): BaseQueryBuilder<TEntity>;
    limit(count: number): BaseQueryBuilder<TEntity>;
    offset(count: number): BaseQueryBuilder<TEntity>;
    getQuery(): SelectQueryBuilder<TEntity>;
}
