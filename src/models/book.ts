import { IsNotEmpty, IsOptional, IsUUID, } from 'class-validator';
import {BaseEntity, Column, Entity, PrimaryColumn, PrimaryGeneratedColumn} from 'typeorm';

@Entity({ name: 'books', schema: 'public' })
export class Book extends BaseEntity {
    @IsUUID('4')
    @PrimaryGeneratedColumn('uuid')
    public id: string;

    @IsNotEmpty()
    @Column()
    public title: string;

    @Column()
    @IsOptional()
    public description: string;

    @IsNotEmpty()
    @Column()
    public author: string;

    @Column()
    @IsOptional()
    public year: number;

    @Column()
    @IsOptional()
    public image: string;

    @IsNotEmpty()
    @Column()
    public publisher: string;
}
