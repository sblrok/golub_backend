import { Container, Service } from 'typedi';
import { createConnection } from 'typeorm';
import { CustomTypeOrmLogger } from './common/CustomTypeOrmLogger';
import { config } from './config';

@Service()
export class DataBase {
    constructor() {}

    public async connect() {
        const connection = await createConnection({
            type: 'postgres',
            host: config.database.host,
            port: config.database.port,
            username: config.database.username,
            password: config.database.password,
            database: config.database.name,
            synchronize: true,
            dropSchema: true,
            migrationsRun: true,
            logging: !config.disableSqlLogs,
            logger: config.disableSqlLogs
                ? null
                : new CustomTypeOrmLogger(query => {
                    return require('sql-formatter').format(query, { language: 'pl/sql' });
                }),
            extra: {
                ssl: config.database.ssl,
                idleTimeoutMillis: config.database.connectionTimeout,
                poolSize: config.database.poolSize
            },
            entities: ['src/models/**/*.ts'],
            migrations: ['src/migrations/**/*.ts'],
            cli: {
                entitiesDir: 'src/models',
                migrationsDir: 'src/migration'
            }
        });

        Container.set('connection', connection);
    }
}
