import {
    Authorized,
    Body,
    Delete,
    Get,
    JsonController,
    OnUndefined,
    Param,
    Post,
    Put,
    QueryParam
} from 'routing-controllers';
import {User} from '@models';
import {UsersService} from '../services/users.service';

@JsonController('/users')
export class UsersController {
    constructor(private usersService: UsersService) {}

    @Get('/')
    @Authorized()
    public find(
        @QueryParam('limit') limit: number,
        @QueryParam('offset') offset: number) {
        return this.usersService.find({limit, offset});
    }

    @Get('/:id([A-Fa-f0-9]{24})')
    @Authorized()
    public findOne(@Param('id') id: string) {
        return this.usersService.findOne({searchBy: {id}});
    }

    @Post('/')
    @Authorized()
    public create(@Body() user: User) {
        return this.usersService.create(user);
    }

    @Put('/:id([A-Fa-f0-9]{24})')
    @Authorized()
    public update(
        @Param('id') id: string,
        @Body() user: User) {
        return this.usersService.update({searchBy: {id}}, user);
    }

    @Delete('/:id([A-Fa-f0-9]{24})')
    @Authorized()
    @OnUndefined(204)
    public delete(@Param('id') id: string) {
        return this.usersService.delete({searchBy: {id}});
    }
}
