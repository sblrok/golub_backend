import {
    Authorized,
    Body,
    Delete,
    Get,
    JsonController,
    OnUndefined,
    Param,
    Post,
    Put,
    QueryParam
} from 'routing-controllers';
import {BooksService} from '../services/books.service';
import {Book} from '@models';

@JsonController('/books')
export class BooksController {
    constructor(private booksService: BooksService) {}

    @Get('/')
    @Authorized()
    public find(
        @QueryParam('limit') limit: number,
        @QueryParam('offset') offset: number) {
        return this.booksService.find({limit, offset});
    }

    @Get('/:id([A-Fa-f0-9]{24})')
    @Authorized()
    public findOne(@Param('id') id: string) {
        return this.booksService.findOne({searchBy: {id}});
    }

    @Post('/')
    @Authorized()
    public create(@Body() book: Book) {
        return this.booksService.create(book);
    }

    @Put('/:id([A-Fa-f0-9]{24})')
    @Authorized()
    public update(
        @Param('id') id: string,
        @Body() book: Book) {
        return this.booksService.update({searchBy: {id}}, book);
    }

    @Delete('/:id([A-Fa-f0-9]{24})')
    @Authorized()
    @OnUndefined(204)
    public delete(@Param('id') id: string) {
        return this.booksService.delete({searchBy: {id}});
    }
}
